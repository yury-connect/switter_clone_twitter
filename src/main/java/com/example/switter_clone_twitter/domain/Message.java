package com.example.switter_clone_twitter.domain;

import javax.persistence.*;


@Entity
public class Message {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    private String text;
    private String tag;

    /*
    Укажем каким образом   author   будет сохраняться в DB
    @ManyToOne - одному пользователю соответствует множество сообщений
        т.е. с Этой стороны @ManyToOne
    Со стороны пользователя - это будет @OneToMany
        т.е. от 1 пользователя к множеству сообщений
    fetch = FetchType.EAGER   каждый раз когда получаем сообщение - мы хотим получать информацию об авторе
    @JoinColumn(name = "user_id")  эта колонка должна быть записана в DB (называться)
        "user_id", а не "author_id". как оно было бы по умолчанию.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")   // эта колонка должна быть записана в DB (называться "user_id", а не "author_id". как оно было бы по умолчанию)
    private User author;



    public Message() {
    }
    public Message(String text, String tag, User user) {
        this.text = text;
        this.tag = tag;
        this.author = user;
    }



    public String getAuthorName() {
        return author != null ? author.getUsername() : "<none>";
    }


    public void setText(String text) {
        this.text = text;
    }
    public String getText() {
        return text;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }
    public void setTag(String tag) {
        this.tag = tag;
    }

    public User getAuthor() {
        return author;
    }
    public void setAuthor(User author) {
        this.author = author;
    }

}