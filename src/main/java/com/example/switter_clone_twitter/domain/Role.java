package com.example.switter_clone_twitter.domain;

import org.springframework.security.core.GrantedAuthority;


// значения Enum и будут имплементациями этого класса
public enum Role implements GrantedAuthority {

    USER;

    @Override
    public String getAuthority() {
        return name();   // строковое представление значения  'USER'
    }

}