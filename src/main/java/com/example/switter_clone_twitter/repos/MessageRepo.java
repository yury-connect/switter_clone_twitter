package com.example.switter_clone_twitter.repos;

import com.example.switter_clone_twitter.domain.Message;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


/** Действуем согласно имнструкции с официальной странички spring.io
 *  со странички guides (https://spring.io/guides/gs/accessing-data-mysql/)
 *  просто скопировали из раздела 'Create the Repository'
 */
public interface MessageRepo extends CrudRepository<Message, Long> {

    List<Message> findByTag(String tag);
    /* добавим метод поиска в DB по tag.
    все правила описаны в Руководство по JPA репозиториям:
       https://www.youtube.com/redirect?event=video_description&q=https%3A%2F%2Fdocs.spring.io%2Fspring-data%2Fjpa%2Fdocs%2F1.5.0.RELEASE%2Freference%2Fhtml%2Fjpa.repositories.html%23jpa.query-methods.query-creation&redir_token=j-7v6kQXfwxrUH8ApsJNiqRf0NN8MTU4ODk2ODE5MEAxNTg4ODgxNzkw&v=nyFLX3q3poY
    */

}