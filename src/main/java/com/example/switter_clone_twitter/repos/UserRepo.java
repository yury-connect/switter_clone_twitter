package com.example.switter_clone_twitter.repos;

import com.example.switter_clone_twitter.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;



public interface UserRepo extends JpaRepository<User, Long> {

    User findByUsername(String username);

}