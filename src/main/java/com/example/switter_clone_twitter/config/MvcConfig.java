package com.example.switter_clone_twitter.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/*
Данный класс MvcConfig просто копируем с официальной странички Spring
https://spring.io/guides/gs/securing-web/
Тут конфигурация нашего WEB - слоя
Системная логика нас вполне устраивает, поэтому просто вешаем ее if шаблон "/login"
Затем добавим зависимости (все из гайда!)
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
    }

}