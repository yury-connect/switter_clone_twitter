package com.example.switter_clone_twitter.config;

import com.example.switter_clone_twitter.service.UserSevice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

/*
Данный класс MvcConfig просто копируем с официальной странички Spring
https://spring.io/guides/gs/securing-web/
Этот класс при старте приложения конфигурирует WebSecurity
Система передает на вход объект, в кот. вкл. авторизацию и т.д.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserSevice userSevice;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .authorizeRequests()
                    .antMatchers("/", "/registration").permitAll() // полный доступ только для главной странички и стр. регистрации
                    .anyRequest().authenticated() // соотв-но для всех остальных требуем авторизацию
                .and()
                    .formLogin()   // вкл. форму  Login
                    .loginPage("/login")   // форма Login на мэпинге "/login"
                    .permitAll()   // разрешаем этим пользоваться всем
                .and()
                    .logout()   // аналогично вкл logout и разреш. польз. всем
                    .permitAll();
    }


    /*
    UserDetailsService будет выдаваться системе по требованию этим методом,
    он создеет менеджер   InMemoryUserDetailsManager(user) , кот обсл. учетные записи
    Олн нужен только для отладки. При каждом перезапуске приложения он заново создает пользователя
    *** каждого пользователя придется добавлять руками. Это не очень удобно! ***
    Отключаем, т.к. будем брать пользователей из DB. см. ниже.
     */
//    @Bean
//    @Override
//    public UserDetailsService userDetailsService() {
//        UserDetails user =
//                User.withDefaultPasswordEncoder()
//                        .username("u")
//                        .password("p")
//                        .roles("USER")
//                        .build();
//
//        return new InMemoryUserDetailsManager(user);
//    }


    // теперь будем пользователей брать из DB
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userSevice)
                .passwordEncoder(NoOpPasswordEncoder.getInstance());
    }

}