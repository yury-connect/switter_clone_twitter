package com.example.switter_clone_twitter.controller;

import com.example.switter_clone_twitter.domain.Role;
import com.example.switter_clone_twitter.domain.User;
import com.example.switter_clone_twitter.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Collections;
import java.util.Map;


@Controller
public class RegistrationController {

    @Autowired
    private UserRepo userRepo;


    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }


    @PostMapping("/registration")
    public String addUser(User user, Map<String, Object> model) {
        User userFromDb = userRepo.findByUsername(user.getUsername());

        if (userFromDb != null) {
            model.put("message", "   Такой пользователь в DB уже есть!   //   User exists!");
            return "registration";   // вернуть страничку регистрации
        }

        user.setActive(true);   // новый пользователь сразу будет активный
        user.setRoles(Collections.singleton(Role.USER));   // ожидается коллекция в виде Set, но т.к. всего 1 знач-е, то создадим Set с одним едитнственным значением
        userRepo.save(user);   // сохраняем пользователя

        return "redirect:/login";   // при успешной авторизации будет редирект на страницу логина
    }

}