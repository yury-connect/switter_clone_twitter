package com.example.switter_clone_twitter.service;

import com.example.switter_clone_twitter.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service   // на самом деле это  Componennt , Service -это просто для информации
public class UserSevice implements UserDetailsService { // интерфейс, из которого возвращаем UserDetails, в нашем случае пользователя

    // используем устаревшую аннотацию В ЦЕЛЯХ ОБУЧЕНИЯ
    @Autowired
    private UserRepo userRepo;


    // на самом деле вместо строчек выше правильнее сделать так:
    // (т.е. спринг когда будет видеть у конструкторе какие-то зависимости, он будет пытаться их сразу заинжектить)
//    private UserRepo userRepo;
//
//    public UserSevice(UserRepo userRepo) {
//        this.userRepo = userRepo;
//    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepo.findByUsername(username);
    }

}